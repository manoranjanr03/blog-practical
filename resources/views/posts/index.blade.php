{{--@extends('posts.layout')--}}
@extends('layouts.app')

@section('content')
    @guest
        <div class="row" style="margin-top: 5rem;">

        </div>
    @else
        <div class="row" style="margin-top: 5rem;">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2></h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('posts.create') }}"> Create New Post</a>
                </div>
            </div>
        </div>
    @endguest


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Details</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Image</th>


            @guest

            @else
                <th width="280px">Action</th>
            @endguest
        </tr>
        @foreach ($data as $key => $value)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $value->title }}</td>
                <td>{{ \Str::limit($value->description, 100) }}</td>
                <td>{{ $value->start_date}}</td>
                <td>{{ $value->end_date}}</td>
                <td> @if ($value->image)
                        <img class="image"  src="{{ URL::to('/') }}/uploads/postimages/{{$value->image}}">
                    @else
                        <img class="profile_image"  src="{{ URL::to('/') }}/uploads/images/noimg.png">
                    @endif
                </td>

                    @guest

                    @else
                    <td>
                        <form action="{{ route('posts.destroy',$value->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('posts.show',$value->id) }}">Show</a>
                            <a class="btn btn-primary" href="{{ route('posts.edit',$value->id) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                    @endguest


            </tr>
        @endforeach
    </table>
    {!! $data->links() !!}
@endsection
