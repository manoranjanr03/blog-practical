Please follow the below setup to configure the project in the local envirnment

1 . Clone the project from the url https://gitlab.com/manoranjanr03/blog-practical.git

2 . run the composer update command inside the project directory to get the vender folder

3. set up the database credentials in the .env file

4. find the database dumb from the project root directory and import or run php artisan migrate to generate the tables

5. run the php artisan serve to check the application
