function validate_registrationform(){
    alert(1);
    $('#regform').validate({
        rules: {
            first_name: {
                required: true,
            },
            lst_name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8
            },
            confirm_password: {
                required: true,
                minlength: 8,
                equalTo: "#password"
            },
            dob: {
                required: true,
            }
        }
    });
}
function validate_loginform(){
    $('#loginform').validate({ // initialize the plugin
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8
            }
        }
    });
}
function validate_postform(){
    $('#postform').validate({ // initialize the plugin
        rules: {
            title: {
                required: true,
            },
            description: {
                required: true,
            },
            start_date: {
                required: true,
            },
            end_date:{
                required: true,
            }
        }
    });
}
