<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except("index");
    }

    public function index()
    {
        try {
            if (Auth::user()) {
                $user_id = Auth::user()->id;
                if(Auth::user()->user_role == '1')
                {
                    $data = Post::latest()->paginate(5);
                }
                else
                {
                    $data = Post::where('user_id', $user_id)->paginate(5);
                }
            } else {
                $data = Post::latest()->paginate(5);
            }



            return view('posts.index',compact('data'))
                ->with('i', (request()->input('page', 1) - 1) * 5);
        } catch (\Exception $e) {
            $data = [
                'exception' => $e->getMessage()
            ];
            Log::info(json_encode($data));
            abort(500);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('posts.create');
        } catch (\Exception $e) {
            $data = [
                'input_params' => $request->all(),

                'exception' => $e->getMessage()
            ];
            Log::info(json_encode($data));
            abort(500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
            ]);

            $post = $request->all();
            $post['user_id']=Auth::user()->id;
            if($request->hasfile('image')){
                $file = $request->file('image');
                if($file){
                    $filename = time()."_".$file->getClientOriginalName();
                    $destinationPath = base_path().'/public/uploads/postimages' ;
                    $file->move($destinationPath,$filename);
                }
                $post['image']=$filename;
            }

            Post::create($post);

            return redirect()->route('posts.index')
                ->with('success','Post created successfully.');
        } catch (\Exception $e) {
            $data = [
                'input_params' => $request->all(),

                'exception' => $e->getMessage()
            ];
            Log::info(json_encode($data));
            abort(500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        try {
            return view('posts.show',compact('post'));

        } catch (\Exception $e) {
            $data = [
                'input_params' => $post->all(),

                'exception' => $e->getMessage()
            ];
            Log::info(json_encode($data));
            abort(500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        try {
            return view('posts.edit',compact('post'));

        } catch (\Exception $e) {
            $data = [
                'input_params' => $post->all(),

                'exception' => $e->getMessage()
            ];
            Log::info(json_encode($data));
            abort(500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        try {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
            ]);
//        $post =  $request->all();
//        $post['user_id']=Auth::user()->id;
            $filename='';
            if($request->hasfile('image')){
                $file = $request->file('image');
                if($file){
                    $filename = time()."_".$file->getClientOriginalName();
                    $destinationPath = base_path().'/public/uploads/postimages' ;
                    $file->move($destinationPath,$filename);
                }
                $prev_path = base_path().'public/uploads/postimages'.$request->get('old_image');
                unlink($prev_path);
                $post['image'] = $filename;
            }else{
                $post['image']  = $request->get('old_image');
            }
//        echo "<pre>";
//        print_r($request->all());print_r($post->toArray());exit;
            $post->update($post->toArray());

            return redirect()->route('posts.index')
                ->with('success','Post updated successfully');
        } catch (\Exception $e) {
            $data = [
                'input_params' => $request->all(),

                'exception' => $e->getMessage()
            ];
            Log::info(json_encode($data));
            abort(500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        try {
            $post->delete();

            return redirect()->route('posts.index')
                ->with('success','Post deleted successfully');
        } catch (\Exception $e) {
            $data = [
                'input_params' => $post->all(),

                'exception' => $e->getMessage()
            ];
            Log::info(json_encode($data));
            abort(500);
        }

    }
}
